<?php
function buscarCadenaEnArray($cadena, $array) {
    foreach ($array as $elemento) {
        if ($elemento === $cadena) {
            return true;
        }
    }
    return false;
}

$miArray = array("Cerro Porteno", "Guarani", "Sportivo Luqueno", "Ameliano", "Libertad", "12 de Octubre", "Trinidense", "Olimpia de Ita", "Boca Juniors", "River Plate");
$cadenaABuscar = "Olimpia";

if (buscarCadenaEnArray($cadenaABuscar, $miArray)) {
    echo "Ya existe $cadenaABuscar";
} else {
    echo "Es Nuevo";
    $miArray[] = $cadenaABuscar;
}

echo "<br>Contenido del array:";
print_r($miArray);
?>
