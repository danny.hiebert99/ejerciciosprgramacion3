
<?php
    include 'excluir/ejercicio14_generar_array.php';
    include 'excluir/ejercicio14_imprimir_array.php';
    include 'excluir/ejercicio14_imprimir_letras.php';

    $array = generarArray();

    echo "Array completo:<br>";
    imprimirVector($array);

    echo "<br>Claves que empiezan con a, d, m o z<br>";
    imprimirLetras($array);
?>
