<?php
function incrementar_visitas($file) {
    if (file_exists($file)) {
        $visitas = file_get_contents($file);
        $visitas++;
    } else {
        $visitas = 1;
    }
    file_put_contents($file, $visitas);
    return $visitas;
}

$contador_file = 'contador.txt';
echo "Número de visitas: " . incrementar_visitas($contador_file);
?>
