<?php
function generarVector() {
    $vector = array();
    for ($i = 0; $i < 100; $i++) {
        $vector[] = rand(1, 1000);
    }
    return $vector;
}

function encontrarMayor($vector) {
    $mayor = $vector[0];
    $indice = 0;
    for ($i = 1; $i < count($vector); $i++) {
        if ($vector[$i] > $mayor) {
            $mayor = $vector[$i];
            $indice = $i;
        }
    }
    return array('valor' => $mayor, 'indice' => $indice);
}

$vector = generarVector();
$mayorElemento = encontrarMayor($vector);
echo "El elemento con índice {$mayorElemento['indice']} posee el mayor valor que {$mayorElemento['valor']}.";
?>
