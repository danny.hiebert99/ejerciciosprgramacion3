<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $edad = $_POST["edad"];

    echo "Nombre: $nombre<br>";
    echo "Apellido: $apellido<br>";
    echo "Edad: $edad años<br>";
} else {
    echo <<<HTML
    <!DOCTYPE html>
    <html>
    <head>
        <title>Datos personales</title>
    </head>
    <body>
        <h2>Formulario Datos Personales</h2>
        <form method="POST" action="">
            <label for="nombre">Nombre:</label>
            <input type="text" name="nombre" required><br><br>

            <label for="apellido">Apellido:</label>
            <input type="text" name="apellido" required><br><br>

            <label for="edad">Edad:</label>
            <input type="number" name="edad" required><br><br>

            <input type="submit" value="Enviar">
        </form>
    </body>
    </html>
HTML;
}
?>