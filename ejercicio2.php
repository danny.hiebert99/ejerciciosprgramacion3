<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Php info</title>
</head>
<body>
<?php
    // Versión de PHP utilizada
    $php_version = phpversion();

    // Id de la versión de PHP
    $php_version_id = PHP_VERSION_ID;

    // Valor máximo soportado para enteros para esa versión
    $max_int = PHP_INT_MAX;

    // Tamaño máximo del nombre de un archivo
    $max_filename_length = PHP_MAXPATHLEN;

    // Versión del Sistema Operativo
    $os_version = php_uname('s') . ' ' . php_uname('r');

    // Símbolo correcto de 'Fin De Línea' para la plataforma en uso
    $eol_symbol = PHP_EOL;

    // Include path por defecto
    $include_path = get_include_path();

    echo "Versión de PHP utilizada: $php_version<br>";
    echo "ID de la versión de PHP: $php_version_id<br>";
    echo "Valor máximo soportado para enteros: $max_int<br>";
    echo "Tamaño máximo del nombre de un archivo: $max_filename_length<br>";
    echo "Versión del Sistema Operativo: $os_version<br>";
    echo "Símbolo correcto de 'Fin De Línea': $eol_symbol <br>";
    echo "Include path por defecto: $include_path<br>";
?>

</body>
</html>