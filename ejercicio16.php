<?php
class Nodo {
    public $valor;
    public $izquierdo;
    public $derecho;

    public function __construct($valor) {
        $this->valor = $valor;
        $this->izquierdo = null;
        $this->derecho = null;
    }
}

class ArbolBinario {
    public $raiz;

    public function __construct($valor) {
        $this->raiz = new Nodo($valor);
    }

    public function preOrden($nodo) {
        if ($nodo !== null) {
            echo $nodo->valor . " "; // Imprime el valor del nodo
            $this->preOrden($nodo->izquierdo); // Recorre el subárbol izquierdo
            $this->preOrden($nodo->derecho); // Recorre el subárbol derecho
        }
    }
}

// Construyendo el árbol
$arbol = new ArbolBinario(3);

$arbol->raiz->izquierdo = new Nodo(6);
$arbol->raiz->derecho = new Nodo(4);

$arbol->raiz->izquierdo->izquierdo = new Nodo(14);
$arbol->raiz->izquierdo->derecho = new Nodo(9);

$arbol->raiz->derecho->izquierdo = new Nodo(900);
$arbol->raiz->derecho->derecho = new Nodo(45);

$arbol->raiz->izquierdo->izquierdo->izquierdo = new Nodo(100);
$arbol->raiz->izquierdo->izquierdo->derecho = new Nodo(30);

$arbol->raiz->izquierdo->derecho->izquierdo = new Nodo(40);
$arbol->raiz->izquierdo->derecho->derecho = new Nodo(15);

echo "Recorrido del arbol en preorden: ";
$arbol->preOrden($arbol->raiz);
?>