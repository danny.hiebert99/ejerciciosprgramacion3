<?php
function generarArray() {
    $array = array();

    for ($i = 0; $i < 10; $i++) {
        $indice = generarStringAleatorio();
        $valor = rand(1, 1000);
        $array[$indice] = $valor;
    }

    return $array;
}

function generarStringAleatorio() {
    $longitud = rand(5, 10);
    $caracteres = 'abcdefghijklmnopqrstuvwxyz';
    $longitudCaracteres = strlen($caracteres);
    $cadenaAleatoria = '';

    for ($i = 0; $i < $longitud; $i++) {
        $indiceAleatorio = rand(0, $longitudCaracteres - 1);
        $cadenaAleatoria .= $caracteres[$indiceAleatorio];
    }

    return $cadenaAleatoria;
}

?>
