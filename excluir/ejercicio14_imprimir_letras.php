<?php
function imprimirLetras($array) {
    $letras = ['a', 'd', 'm', 'z'];
    $encontrado = false;

    foreach ($array as $indice => $valor) {
        $primeraLetra = substr($indice, 0, 1);

        if (in_array($primeraLetra, $letras)) {
            echo "Clave que comienza con $primeraLetra: $indice<br>";
            $encontrado = true;
        }
    }

    if (!$encontrado) {
        echo "No se encontraron claves que empiecen con a, d, m o z.<br>";
    }
}
?>
