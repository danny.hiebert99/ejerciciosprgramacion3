<?php
function buscarPersona($nombre, $apellido) {
    $archivo = fopen("agenda.txt", "r");
    while(!feof($archivo)) {
        $linea = trim(fgets($archivo));
        if($linea == "$nombre $apellido") {
            fclose($archivo);
            return true;
        }
    }
    fclose($archivo);
    return false;
}

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    $nombre = $_POST['nombre'];
    $apellido = $_POST['apellido'];
    if(buscarPersona($nombre, $apellido)) {
        echo "Persona encontrada";
    } else {
        echo "Persona no encontrada";
    }
}
?>

<form method="post">
    Nombre: <input type="text" name="nombre"><br>
    Apellido: <input type="text" name="apellido"><br>
    <input type="submit" value="Buscar">
</form>
