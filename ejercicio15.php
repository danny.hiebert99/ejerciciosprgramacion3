<?php

// Función para generar un arreglo de 20 elementos con valores aleatorios de 1 a 10
function generarVector() {
    $vector = [];
    for ($i = 0; $i < 20; $i++) {
        $vector[] = rand(1, 10);
    }
    return $vector;
}

// Función recursiva para imprimir el arreglo de atrás hacia adelante
function imprimirArregloReversa($arreglo, $indice) {
    if ($indice >= 0) {
        echo $arreglo[$indice] . ", ";
        imprimirArregloReversa($arreglo, $indice - 1);
    }
}

// Generar el vector
$vector = generarVector();

// Imprimir el vector generado
echo "Vector generado: ";
foreach ($vector as $elemento) {
    echo $elemento . ", ";
}
echo "<br>";

// Llamar a la función recursiva para imprimir el vector de atrás hacia adelante
echo "Vector de atrás hacia adelante: ";
imprimirArregloReversa($vector, count($vector) - 1);

?>
