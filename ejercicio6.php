<!DOCTYPE html>
<html>
<head>
    <title>Calculadora PHP</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            text-align: center;
            background-color: #f5f5f5;
        }
        h1 {
            color: #333;
        }
        form {
            background-color: #fff;
            border-radius: 5px;
            padding: 20px;
            width: 300px;
            margin: 0 auto;
        }
        input[type="text"], select, input[type="submit"] {
            display: block;
            margin: 10px 0;
            padding: 5px;
            width: 100%;
        }
    </style>
</head>
<body>
    <h1>Calculadora PHP</h1>
    <form method="post">
        <label for="operand1">Operando 1:</label>
        <input type="text" name="operand1" required>
        <label for="operand2">Operando 2:</label>
        <input type="text" name="operand2" required>
        <label for="operation">Operación:</label>
        <select name="operation">
            <option value="suma">Suma</option>
            <option value="resta">Resta</option>
            <option value="multiplicacion">Multiplicación</option>
            <option value="division">División</option>
        </select>
        <input type="submit" value="Calcular">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $operand1 = $_POST["operand1"];
        $operand2 = $_POST["operand2"];
        $operation = $_POST["operation"];

        if (!is_numeric($operand1) || !is_numeric($operand2)) {
            echo "Ambos operandos deben ser números.";
        } else {
            switch ($operation) {
                case "suma":
                    $result = $operand1 + $operand2;
                    break;
                case "resta":
                    $result = $operand1 - $operand2;
                    break;
                case "multiplicacion":
                    $result = $operand1 * $operand2;
                    break;
                case "division":
                    if ($operand2 == 0) {
                        echo "La división por cero no está permitida.";
                    } else {
                        $result = $operand1 / $operand2;
                    }
                    break;
                default:
                    echo "Operación no válida.";
            }

            if (isset($result)) {
                echo "Resultado: $result";
            }
        }
    }
    ?>
</body>
</html>
