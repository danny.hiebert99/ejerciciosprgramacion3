<?php
$n = 5;
$m = 10;
function generarMatrizAleatoria($n, $m) {
    $matriz = array();
    for ($i = 0; $i < $n; $i++) {
        $fila = array();
        for ($j = 0; $j < $m; $j++) {
            $fila[] = rand(1, 9);
        }
        $matriz[] = $fila;
    }
    return $matriz;
}
function imprimirMatriz($matriz) {
    echo "<table border='1'>";
    foreach ($matriz as $fila) {
        echo "<tr>";
        foreach ($fila as $valor) {
            echo "<td>$valor</td>";
        }
        echo "</tr>";
    }
    echo "</table>";
}
$matriz = generarMatrizAleatoria($n, $m);
imprimirMatriz($matriz);
?>