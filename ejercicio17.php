<?php

    $tree = array(
        "R" => array(
            "S" => array(
                "U" => array(
                    "L" => array(),
                    "Q" => array(),
                    "W" => array()
                ),
                "V" => array()
            ),
            "T" => array(
                "G" => array(
                    "C" => array(),
                    "K" => array()
                ),
                "F" => array()
            ),
            "D" => array(
                "H" => array(
                    "A" => array(),
                    "X" => array()
                ),
                "J" => array(
                    "Z" => array(),
                    "I" => array()
                )
            )
        )
    );


    function imprimirArbol($nodo, $nivel = 0) {
        foreach ($nodo as $clave => $hijos) {
            echo str_repeat('--', $nivel) . $clave . "<br>";
            if (!empty($hijos)) {
                imprimirArbol($hijos, $nivel + 1);
            }
        }
    }


    imprimirArbol($tree);

?>