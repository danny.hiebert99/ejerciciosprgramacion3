<?php
function generarVector() {
    $vector = array();
    for ($i = 0; $i < 100; $i++) {
        $vector[] = rand(1, 100);
    }
    return $vector;
}
function sumarVector($vector) {
    $suma = 0;
    foreach ($vector as $valor) {
        $suma += $valor;
    }
    return $suma;
}
$vector = generarVector();
$suma = sumarVector($vector);
echo "La sumatoria de los elementos es: $suma";
?>