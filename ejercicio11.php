<?php
$urls = array(
    'https://drive.google.com/',
    'https://classroom.google.com/',
    'https://www.abc.com.py/',
    'https://d10.ultimahora.com/',
);

function contarAccesos($urls) {
    $contador = array();
    foreach ($urls as $url) {
        if (array_key_exists($url, $contador)) {
            $contador[$url]++;
        } else {
            $contador[$url] = 1;
        }
    }
    return $contador;
}

$conteoAccesos = contarAccesos($urls);
echo "Conteo de accesos a cada URL:<br>";
foreach ($conteoAccesos as $url => $accesos) {
    echo "$url&nbsp;&nbsp;&nbsp;Accesos: $accesos veces<br>";
}
?>
