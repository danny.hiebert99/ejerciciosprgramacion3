<?php
function procesar_datos($input_file, $output_file) {
    $lines = file($input_file);
    $output = '';
    foreach ($lines as $line) {
        $data = explode(' ', trim($line));
        $matricula = $data[0];
        $sumatoria_notas = $data[3] + $data[4] + $data[5];
        $output .= "$matricula $sumatoria_notas\n";
    }
    file_put_contents($output_file, $output);
}

procesar_datos('origen.txt', 'generado.txt');
?>
