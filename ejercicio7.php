<!DOCTYPE html>
<html>
<head>
    <title>Verificar Palíndromo</title>
</head>
<body>
    <h1>Verificador de Palindromo</h1>
    <form method="post">
        <label for="cadena">Ingrese una cadena: </label>
        <input type="text" name="cadena" id="cadena">
        <input type="submit" value="Verificar">
    </form>

    <?php
        function esPalindromo($cadena) {
            $cadena = str_replace(' ', '', strtolower($cadena));
            $cadenaInvertida = strrev($cadena);
            if ($cadena == $cadenaInvertida) {
                return true;
            } else {
                return false;
            }
        }

        if (isset($_POST['cadena'])) {
            $cadena = $_POST['cadena'];
            if (esPalindromo($cadena)) {
                echo "La cadena '$cadena' es un palíndromo.";
            } else {
                echo "La cadena '$cadena' no es un palíndromo.";
            }
        }
    ?>
</body>
</html>
