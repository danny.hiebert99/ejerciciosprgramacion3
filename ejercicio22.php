<?php
function verificarUsuario($usuario, $password) {
    $archivo = fopen("usuarios.txt", "r");
    while(!feof($archivo)) {
        $linea = trim(fgets($archivo));
        list($user, $pass) = explode(',', $linea);
        if($user == $usuario && $pass == $password) {
            fclose($archivo);
            return true;
        }
    }
    fclose($archivo);
    return false;
}

$mensaje = '';

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    $usuario = $_POST['usuario'];
    $password = $_POST['password'];
    if(verificarUsuario($usuario, $password)) {
        $mensaje = '¡Bienvenido al sistema!';
    } else {
        $mensaje = 'Error de acceso';
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
</head>
<body>
    <?php if ($mensaje): ?>
        <p><?php echo $mensaje; ?></p>
    <?php else: ?>
        <form action="index.php" method="post">
            Usuario: <input type="text" name="usuario"><br>
            Contraseña: <input type="password" name="password"><br>
            <input type="submit" value="Login">
        </form>
    <?php endif; ?>
</body>
</html>
