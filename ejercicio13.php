<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Array asociativo</title>
</head>
<body>
<?php
    // Crear un array asociativo de 10 elementos con valores strings
    $array_10_elementos = array(
        "clave1" => "valor1",
        "clave2" => "valor2",
        "clave3" => "valor3",
        "clave4" => "valor4",
        "clave5" => "valor5",
        "clave6" => "valor6",
        "clave7" => "valor7",
        "clave8" => "valor8",
        "clave9" => "valor9",
        "clave10" => "valor10"
    );

    // Crear un array asociativo de un elemento
    $array_1_elemento = array(
        "clave11" => "valor11"
    );

    // Verificar si existe la clave del array de un elemento en el array de 10 elementos
    $clave_elemento = array_keys($array_1_elemento)[0]; // Obtener la clave del array de un elemento
    if (array_key_exists($clave_elemento, $array_10_elementos)) {
        echo "La clave '$clave_elemento' existe en el array de 10 elementos.<br>";
    } else {
        echo "La clave '$clave_elemento' no existe en el array de 10 elementos.<br>";
    }

    // Verificar si existe el valor del vector de un elemento en el vector de 10 elementos
    $valor_elemento = reset($array_1_elemento); // Obtener el valor del array de un elemento
    if (in_array($valor_elemento, $array_10_elementos)) {
        echo "El valor '$valor_elemento' existe en el array de 10 elementos.<br>";
    } else {
        echo "El valor '$valor_elemento' no existe en el array de 10 elementos.<br>";
    }

    // Si no existe ni la clave ni el valor en el vector mayor, se inserta como un elemento más
    if (!array_key_exists($clave_elemento, $array_10_elementos) && !in_array($valor_elemento, $array_10_elementos)) {
        $array_10_elementos[$clave_elemento] = $valor_elemento;
        echo "Se ha insertado la clave '$clave_elemento' con el valor '$valor_elemento' en el array de 10 elementos.<br>";
    }

    // Imprimir el array de 10 elementos después de los posibles cambios
    echo implode(' - ', $array_10_elementos) . "<br>";
?>

</body>
</html>