<!DOCTYPE html>
<html>
<head>
    <title>Pares</title>
</head>
<body>
    <?php
    define("N", 59);

    echo "<table border='1'>";
    echo "<tr><th>Números Pares entre 1 y 59</th></tr>";

    for ($i = 2; $i <= N; $i += 2) {
        echo "<tr><td>$i</td></tr>";
    }

    echo "</table";
    ?>
</body>
</html>
