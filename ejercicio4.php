<?php
mt_srand(time());
$numero1 = mt_rand(50, 900);
$numero2 = mt_rand(50, 900);
$numero3 = mt_rand(50, 900);

$numeros = array($numero1, $numero2, $numero3);

rsort($numeros);

echo '<p>';
foreach ($numeros as $numero) {
    if ($numero == $numeros[0]) {
        echo '<span style="color: green;">' . $numero . '</span> ';
    } elseif ($numero == $numeros[count($numeros) - 1]) {
        echo '<span style="color: red;">' . $numero . '</span> ';
    } else {
        echo $numero . ' ';
    }
}
echo '</p>';
?>
